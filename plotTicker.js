// plotTicker.js
// provided code to plot ticker data on a graph
//  (students need not read or edit this file)
// Hany Farid, April 2016

function getData( ticker ) {
	var price = new Array();
	var filename = ticker + '.txt';
	$.ajax({
		url: filename,
		type: 'get',
		dataType: 'html',
		async: false,
		success: function(data) {
			price = data.split('\n').map(Number);
			price.pop(); // delete last empty entry
			if( price.length != 251 && price.length != 252)  { // hack
				price = []; // didn't find ticker data, return empty array
			}
		}
	});
	return price;
}

function plotTicker( ticker, price, minval, maxval, avgval ) {
	var PLOTTER = document.getElementById('plotter');
	PLOTTER.innerHTML = "";

	if( price.length == 0 ) {
		Plotly.newPlot( PLOTTER, [] ); // create a new empty plot
		PLOTTER.innerHTML = "ticker data not found";
		return;
	}

	// build array for horizontal axis
	var time = new Array( price.length );
	for( var i=0; i<price.length; i++ ) {
		time[i] = price.length-i; // flip: price data is in reverse chronological order
	}

	// plot everything
	Plotly.newPlot( PLOTTER, [] ); // create a new empty plot
	var trace1 = {x: time, y: price, name: ticker};
	var trace2 = {x: [0, price.length], y: [avgval, avgval], mode: 'lines', line: {width:0.5}, name: 'avg'};
	var trace3 = {x: [0, price.length], y: [minval, minval], mode: 'lines', line: {width:0.5}, name: 'min'};
	var trace4 = {x: [0, price.length], y: [maxval, maxval], mode: 'lines', line: {width:0.5}, name: 'max'};

	var data = [ trace1, trace2, trace3, trace4 ];

	var layout = {
		title: ticker,
		height: 300,
		width: 600
	};
	Plotly.newPlot( PLOTTER, data, layout );

	//Plotly.plot( PLOTTER, [{ x: time, y: price, name: ticker }], { margin: { t: 0 } } );
	//Plotly.plot( PLOTTER, [{x: [0, price.length], y: [avgval, avgval], mode: 'lines', line: {width:0.5}, name: 'avg', showlegend: true }] );
	//Plotly.plot( PLOTTER, [{x: [0, price.length], y: [minval, minval], mode: 'lines', line: {width:0.5}, name: 'min', showlegend: true }] );
	//Plotly.plot( PLOTTER, [{x: [0, price.length], y: [maxval, maxval], mode: 'lines', line: {width:0.5}, name: 'max', showlegend: true }] );
}
