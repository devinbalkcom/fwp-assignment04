// ticker.js
// April 2016
//  (your name here)


// main function to compute various statistics and call plotTicker to plot
var ticker = function() {
	// get the string that holds the ticker name from the text box.
	//  (e.g, APPL for Apple)
	var ticker = document.getElementById('ticker').value;

	// load the ticker data into the price array using the getData function
	//  from the provided plotTicker.js library
	var price = getData( ticker );

	// your code goes here:

};
